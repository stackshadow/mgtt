package client

import (
	"gitlab.com/mgtt/internal/mgtt/plugin"
)

type Plugin struct {
	broker plugin.Broker
	config configStruct
}

// LocalInit will init the auth-plugin and register it
func Create(broker plugin.Broker) (plugin Plugin) {
	ValidateFeatures(&plugin)

	// store broker
	plugin.broker = broker

	return
}

func (plugin Plugin) Name() string {
	return "client"
}
