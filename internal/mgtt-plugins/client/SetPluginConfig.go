package client

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/client"
	"gitlab.com/mgtt/internal/mgtt/config"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
	"gopkg.in/yaml.v2"
)

func (plugin *Plugin) SetPluginConfig(pluginData interface{}) (changedConfig interface{}, enabled bool) {

	// load from interface
	pluginConfigBytes, err := yaml.Marshal(pluginData)
	utils.PanicOnErr(err)

	// load to config
	err = yaml.Unmarshal(pluginConfigBytes, &plugin.config)
	utils.PanicOnErr(err)

	log.Info().Msg("Loaded config")

	// connect to provided plugins
	if plugin.config.Enable {
		for _, curRemote := range plugin.config.Remotes {
			newClient := &client.MgttClient{}
			newClient.KeepAliveSetTimeout(uint16(config.Globals.Timeout.Seconds()))
			newClient.Connect(curRemote.URL)
			go newClient.PingLoop()
		}
	}

	// is enabled ?
	enabled = plugin.config.Enable
	return
}
