package auth

import (
	"encoding/base64"

	"golang.org/x/crypto/bcrypt"
)

type configUserStruct struct {
	Username string   `yaml:"username,omitempty" json:"username,omitempty"`
	Password string   `yaml:"password" json:"password,omitempty"`
	Groups   []string `yaml:"groups" json:"groups"`
}

// PasswordSet will convert the password-field to bcrypted-password
func (user *configUserStruct) PasswordSet(newPassword string) (err error) {
	var bcryptedData []byte
	bcryptedData, err = bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	user.Password = base64.StdEncoding.EncodeToString(bcryptedData)
	return
}

func (user *configUserStruct) PasswordCheck(password string) (match bool) {

	basswordBytes, err := base64.StdEncoding.DecodeString(user.Password)
	if err == nil {
		errCompare := bcrypt.CompareHashAndPassword(basswordBytes, []byte(password))
		if errCompare == nil {
			match = true
		}
	}

	return
}
