package auth

import (
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var testConfig = configStruct{

	Enable: true,
	Anonym: false,
	New: []configUserStruct{
		{
			Username: "first",
			Password: "firstsecret",
			Groups:   []string{"auth", "debugging"},
		}, {
			Username: "second",
			Password: "secondsecret",
			Groups:   []string{"debugging"},
		},
	},
}

func TestConfigLoad(t *testing.T) {
	// setup logger
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	plugin := Create(nil)
	_, pluginEnabled := plugin.SetPluginConfig(testConfig)

	if !pluginEnabled {
		t.FailNow()
	}

	// check of config exist
	if isOkay := plugin.config.CheckPassword("first", "firstsecret"); isOkay == false {
		t.FailNow()
	}
	if isOkay := plugin.config.CheckPassword("second", "secondsecret"); isOkay == false {
		t.FailNow()
	}
}
