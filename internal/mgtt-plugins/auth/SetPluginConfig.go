package auth

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
	"gopkg.in/yaml.v2"
)

func (plugin *Plugin) SetPluginConfig(pluginData interface{}) (changedConfig interface{}, enabled bool) {

	// load from interface
	pluginConfigBytes, err := yaml.Marshal(pluginData)
	utils.PanicOnErr(err)

	// load to config
	err = yaml.Unmarshal(pluginConfigBytes, &plugin.config)
	utils.PanicOnErr(err)

	// add new users
	for _, newUser := range plugin.config.New {
		if newUser.Username != "" && newUser.Password != "" {
			log.Debug().Str("username", newUser.Username).Msg("found new user, store it to config")
			newUsername := newUser.Username
			_, err = plugin.config.SetUser(newUsername, &newUser.Password, &newUser.Groups)
			utils.PanicOnErr(err)

			changedConfig = plugin.config
		}
	}

	// clear the new-users
	plugin.config.New = []configUserStruct{}

	log.Info().Msg("Loaded config")

	// is enabled ?
	enabled = plugin.config.Enable

	return
}
