
== Config

[source,yaml]
----

plugins:
  auth:

    # set this to true to enable the plugin
    enable: true

    # set this to true to allow anonymouse login beside the username-password
    anonym: false


    # This will create a new user and bcrypt the password
    # this entry will be removed !
    new:
      - username: ANewUser
        password: cleartextpassword
        groups:
          - auth
          - debugging

    # These are the user ( with bcrypted password )
    users:
      john:
        password: JDJhJDEwJFhNNU5BMWhQOUVTQ0pUdktobUtKcnU5YmNCaXphRThqTS5vTDZPa2d4UXVyZ1dZNHNBVlJX
        groups:
        - auth
        - debugging

----

=== New user
- you can add a new user to the file and the password will be automatically crypted with bcrypt. The user will be immediately active
- also the cleartext-password will be deleted
- you can create a new user inside the config-file.

==== Via nix-shell

This will create a hashed password

[source,bash]
----
nix-shell --command "mgtt -p 'test'"
----

==== Via docker

include::../../../build/nix/docker.adoc[tag=password]
