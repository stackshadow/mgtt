package acl

import "gitlab.com/mgtt/internal/mgtt/plugin"

type features interface {
	plugin.V2
	plugin.V2MessageFilter
}

// ValidateFeatures does nothing
//
// just force the compiler to check that we implement the functions we want
func ValidateFeatures(plugin features) {}
