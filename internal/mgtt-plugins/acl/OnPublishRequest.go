package acl

import (
	"fmt"

	"github.com/rs/zerolog/log"
)

// PublishToBrokerPossible get called when an publisher try to publish to the broker
func (plugin Plugin) PublishToBrokerPossible(clientID string, username string, topic string) (accepted bool) {
	var err error

	// check global permission
	allowedGlobally := plugin.checkACL(clientID, "_global", topic, "w")

	// check for specific username
	allowed := plugin.checkACL(clientID, username, topic, "w") || allowedGlobally

	if !allowed {
		log.Warn().Str("topic", topic).Msg("Not allowed")

		if plugin.broker != nil {
			err = plugin.broker.PublishToClient(
				clientID,
				"$SYS/self/error",
				[]byte(fmt.Sprintf("Access to '%s' denied", topic)),
				false,
				0,
			)
		}

	}

	if err != nil {
		log.Error().Err(err).Send()
	}

	return allowed
}
