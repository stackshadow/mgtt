package acl

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
	"gopkg.in/yaml.v2"
)

func (plugin *Plugin) SetPluginConfig(pluginData interface{}) (changedConfig interface{}, enabled bool) {

	// load from interface
	pluginConfigBytes, err := yaml.Marshal(pluginData)
	utils.PanicOnErr(err)

	// load to config
	err = yaml.Unmarshal(pluginConfigBytes, &plugin.config)
	utils.PanicOnErr(err)

	log.Info().Msg("Loaded config")

	// is enabled ?
	enabled = plugin.config.Enable

	return
}
