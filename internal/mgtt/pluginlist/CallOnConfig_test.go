package pluginlist

import (
	"testing"

	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
	"gopkg.in/yaml.v2"
)

type testPluginConfig struct {
	Enable   bool   `yaml:"enable"`
	TestData string `yaml:"data"`
}

type testPluginStruct struct{}

func (plugin testPluginStruct) Name() string {
	return "testPluginConfig"
}

// create the plugin
func (plugin testPluginStruct) SetPluginConfig(pluginData interface{}) (changedConfig interface{}, enabled bool) {

	// get data as struct
	var testPluginConfigData testPluginConfig
	tempData, err := yaml.Marshal(pluginData)
	utils.PanicOnErr(err)
	yaml.Unmarshal(tempData, &testPluginConfigData)

	// we check it
	if testPluginConfigData.Enable != true {
		utils.PanicWithString("Can not parse config")
	}

	enabled = testPluginConfigData.Enable

	// we alter the config
	testPluginConfigData.TestData = "altered"

	// push it back
	// this is needed, so that the plugin-system can store your config !
	tempData, err = yaml.Marshal(testPluginConfigData)
	utils.PanicOnErr(err)
	yaml.Unmarshal(tempData, &changedConfig)

	return

}

func TestPluginConfig(t *testing.T) {

	// create a new plugin
	var testPlugin testPluginStruct

	// create a new list
	pluginList := Create()

	// we prepare the plugins
	var PluginConfigs map[string]interface{} = make(map[string]interface{})
	PluginConfigs[testPlugin.Name()] = testPluginConfig{Enable: true}

	// register the plugin
	pluginList.Register(testPlugin)

	// call all plugins
	pluginList.SendPluginConfig(PluginConfigs)

	// now we start to check if the plugin altered the config
	var testPluginConfigData testPluginConfig
	tempData, err := yaml.Marshal(PluginConfigs[testPlugin.Name()])
	utils.PanicOnErr(err)
	yaml.Unmarshal(tempData, &testPluginConfigData)

	if testPluginConfigData.TestData != "altered" {
		utils.PanicWithString("Testdata was not corret changed by plugin")
	}
}
