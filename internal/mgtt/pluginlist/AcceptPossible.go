package pluginlist

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/plugin"
)

// AcceptPossible ask all plugins if a new client is acceptable
//
// Only if all plugins return true, the connection is accepted
func (s Store) ConnectPossible(clientID string, username string, password string) (accepted bool) {

	//per default we should connect
	accepted = true

	for pluginName, currentPluginInterface := range s {

		if currentPlugin, ok := currentPluginInterface.(plugin.V2ClientFilter); ok {
			log.Debug().Str("plugin", pluginName).Msg("call ConnectPossible")
			pluginAccept := currentPlugin.ConnectPossible(clientID, username, password)

			if !pluginAccept {
				accepted = false
				log.Warn().
					Str("cid", clientID).
					Str("plugin", pluginName).
					Str("username", username).
					Msg("plugin not accept this client")
			}
		}

	}

	return
}
