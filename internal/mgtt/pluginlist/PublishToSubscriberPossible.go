package pluginlist

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/plugin"
)

// CallOnSendToSubscriberRequest  ask all plugins if the message is allowed to publish to an subscriber
//
// calls OnSendToSubscriberRequest on all plugins
//
// this is primary for plugins that checks if another client is allowed
// to receive an message
func (s Store) PublishToSubscriberPossible(clientID string, username string, publishTopic string) (accepted bool) {

	//per default we should accept
	accepted = true

	for pluginName, currentPluginInterface := range s {

		if currentPlugin, ok := currentPluginInterface.(plugin.V2MessageFilter); ok {
			log.Debug().Str("plugin", pluginName).Msg("call OnAcceptNewClient")
			pluginAccept := accepted && currentPlugin.PublishToSubscriberPossible(clientID, username, publishTopic)

			if !pluginAccept {
				accepted = false

				log.Warn().
					Str("cid", clientID).
					Str("plugin", pluginName).
					Str("topic", publishTopic).
					Msg("plugin not accept send to another client")
			}
		}
	}

	return
}
