package pluginlist

import (
	"encoding/json"

	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

// SendPluginConfig send the plugin specific config to all plugins
//
// calls SetPluginConfig on all plugins
func (s Store) SendPluginConfig(pluginConfigs map[string]interface{}) (configChanged bool) {

	for pluginName, currentPluginInterface := range s {

		pluginData := pluginConfigs[pluginName]

		// if no plugin-config is there, we send an empty one
		// this is needed to disable plugins by default
		if pluginData == nil {
			err := json.Unmarshal([]byte("{}"), &pluginData)
			utils.PanicOnErr(err)
		}

		log.Debug().Str("plugin", pluginName).Msg("SetPluginConfig Call")

		changedConfig, pluginEnabled := currentPluginInterface.SetPluginConfig(pluginData)

		if pluginEnabled {
			if changedConfig != nil {
				pluginConfigs[pluginName] = changedConfig
				configChanged = true
				log.Debug().Str("plugin", pluginName).Msg("plugin changed the config")
			}
		} else {
			s.DeRegister(pluginName)
		}

	}

	return
}
