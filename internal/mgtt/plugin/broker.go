package plugin

// Broker represents the broker-functions a plugin expects
type Broker interface {
	Publish(topic string, payload []byte, retain bool, QoS byte) (err error)
	PublishToClient(clientID, topic string, payload []byte, retain bool, QoS byte) (err error)
}
