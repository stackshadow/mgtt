package plugin

// flow
// OnAcceptNewClient -> OnConnected -> OnDisconnected

// V2 is the common interface
//
// this is the minimal set of functions an plugin must have
type V2 interface {
	Name() string
	SetPluginConfig(interface{}) (changedConfig interface{}, enabled bool)
}

type V2ClientFilter interface {
	// ConnectPossible gets called, when a CONNECT-Packet arrived but is not yet added to the list of known clients
	//
	// if this function return false, the client will not added to the known-client-list and get disconnected with return code "not authorized"
	ConnectPossible(clientID string, username string, password string) bool
}

type V2ConnectNotify interface {
	// OnConnected will called after connection is established from an subscriber on the broker
	OnConnected(clientID string)

	// OnDisconnected called, when a client leaves our list of clients
	OnDisconnected(clientID string)
}

type V2SubscriptionFilter interface {
	// SubscriptionPossible ask if an plugin accept the subscription
	// return false if plugin denied to subscribe
	SubscriptionPossible(clientID string, username string, subscriptionTopic string) bool
}

// V2MessageFilter is the capability to decide if a message
// can be published to the broker/client
type V2MessageFilter interface {

	// PublishToBrokerPossible ask  if the message is allowed to publish to the broker
	PublishToBrokerPossible(clientID string, username string, topic string) bool

	// CallOnSendToSubscriberRequest ask if the message is allowed to publish to an subscriber
	//
	// if an plugin set it to false, the message will NOT be sended to clientID
	//
	// clientID: The clientID of the target client
	// username: The username of the target client
	// publishTopic: The topic the broker try to publish to the subscriber
	PublishToSubscriberPossible(clientID string, username string, publishTopic string) bool
}

type V2MessageHandler interface {

	// HandleMessage will handle a message
	//
	// If this function return true, the plugin handled the message and no other plugin will get it
	//
	// If a plugin handle the message, it will NOT sended to other subscribers
	HandleMessage(originClientID string, topic string, payload []byte) (handled bool)
}

// V1 represents an plugin in version 1
type V1 struct {
	broker *Broker // a pointer to the current broker, so we can publish messages to all other clients

	// OnConfig gets called when the app loads or the configfile was changed
	OnPluginConfig func(*interface{}) (configChanged bool)

	// OnNewClient gets called when a new client is incoming
	OnNewClient func(remoteAddr string)

	// OnAcceptNewClient gets called, when a CONNECT-Packet arrived but is not yet added to the list of known clients
	//
	// if this function return false, the client will not added to the known-client-list and get disconnected with return code "not authorized"
	OnAcceptNewClient func(clientID string, username string, password string) bool

	// OnConnected will called after connection is established from an subscriber on the broker
	OnConnected func(clientID string)

	// OnDisconnected called, when a client leaves our list of clients
	OnDisconnected func(clientID string)

	// OnConnack will called after a conack was received
	OnConnack func(clientID string)

	// OnSubscriptionRequest gets called, when an client request an subscription
	// return false if plugin request an abort of an subscription
	OnSubscriptionRequest func(clientID string, username string, subscriptionTopic string) bool

	// OnPublishRequest get called when an publisher try to publish to the broker
	//
	// If an plugin return false, connection will be closed
	OnPublishRequest func(clientID string, username string, topic string) bool

	// OnHandleMessage gets called after OnPublishRequest
	//
	// If this function return true, the plugin handled the message and no other plugin will get it
	//
	// If a plugin handle the message, it will NOT sended to subscribers
	OnHandleMessage func(originClientID string, topic string, payload []byte) (handled bool)

	// OnSendToSubscriberRequest get called when the broker try to publish a message to an subscriber
	//
	// if an plugin set it to false, the message will NOT be sended to clientID
	//
	// This function gets called BEFORE check if the subscriber subscribe to the topic
	//
	// clientID: The clientID of the target client
	// username: The username of the target client
	// publishTopic: The topic the broker try to publish to the subscriber
	OnSendToSubscriberRequest func(clientID string, username string, publishTopic string) bool
}
