package fails

type Fail struct {
	original      error  // the original error
	statusOnError int    // you can override the status code
	clientID      string // the clientID where the error occurred
}

func (err Fail) Error() string {
	return err.original.Error()
}

func (err Fail) ClientID() string {
	return err.clientID
}

func PanicOnErr(err error) {
	if err != nil {
		panic(Fail{
			original: err,
		})
	}
}

func PanicOnClientErr(err error, clientID string) {
	if err != nil {
		panic(Fail{
			original: err,
			clientID: clientID,
		})
	}
}
