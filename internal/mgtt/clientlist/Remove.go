package clientlist

import "github.com/rs/zerolog/log"

// Remove will remove an client with the given clientID and disconnects it
func (l *List) Remove(clientID string) {

	// find the client
	l.writeLock.Lock()
	defer l.writeLock.Unlock()

	if client, exist := l.list[clientID]; exist {
		client.Close()
		delete(l.list, clientID)
		log.Debug().Str("client", clientID).Msg("Client remove from the list")
	} else {
		log.Warn().Str("client", clientID).Msg("Can not remove client from the list, it not exist on the list")
	}

}
