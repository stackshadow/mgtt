package clientlist

import (
	"errors"
)

// PubrelToClient send an PUBREL to an specific client
func (l *List) PubrelToClient(clientID string, MessageID uint16) (err error) {

	// find the client
	l.writeLock.Lock()
	client := l.list[clientID]
	l.writeLock.Unlock()

	if client != nil {
		err = client.SendPubrel(MessageID) // MQTT-2.3.1-6
	} else {
		err = errors.New("Client with id not exist")
	}

	return
}
