package clientlist

// Get will return an mgttClient from an given clientID
func (l *List) Get(clientID string) (existingClient Client) {
	existingClient, _ = l.list[clientID]
	return
}
