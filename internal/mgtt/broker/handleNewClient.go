package broker

import (
	"net"
	"runtime"

	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/client"
	"gitlab.com/mgtt/internal/mgtt/config"
	"gitlab.com/mgtt/internal/mgtt/fails"
)

func (broker *Broker) handleNewClient(newConnection net.Conn) {

	// create a new client
	newClient := &client.MgttClient{}
	newClient.Init(newConnection, config.Globals.Timeout)
	broker.clients.Add(newClient)

	// recover
	defer func() {
		clientID := newClient.ID()

		if r := recover(); r != nil {

			var stackTrace []byte = make([]byte, 1024)
			runtime.Stack(stackTrace, true)

			switch v := r.(type) {
			case fails.Fail:

				log.Error().Stack().Str("cid", clientID).Msg(v.Error())

			case error:
				log.Error().Bytes("stack", stackTrace).Msg(v.Error())
			default:
				log.Error().Bytes("stack", stackTrace).Send()
			}
		}

		if broker.clients.Exist(clientID) {
			newClient := broker.clients.Get(clientID)
			lastWillPacket := newClient.LastWillGet()

			// MQTT-3.1.2-8
			// last-Will-message
			if lastWillPacket != nil {
				log.Info().EmbedObject(newClient).
					Str("topic", lastWillPacket.TopicName).
					Msg("client has a last will, publish it")

				broker.handlePacketsForBroker(newClient.(*client.MgttClient), lastWillPacket)

			}

			newClient.Close()
			broker.clients.Remove(clientID)

			// inform our plugins
			broker.plugins.SendDisconnected(clientID)
		}

	}()

	var err error

	broker.plugins.SendConnected(newClient.RemoteAddr())  // inform the plugins
	defer broker.plugins.SendDisconnected(newClient.ID()) // inform our plugins

	// start communication
	incomingPackages, commDone := newClient.Communicate()

	// do communication
loop:
	for {

		select {
		case packetReceived, ok := <-incomingPackages:
			if ok {
				var normalClose bool
				normalClose, err = broker.handlePacketsForBroker(newClient, packetReceived)
				fails.PanicOnClientErr(err, newClient.ID())

				if normalClose {
					break loop
				}
			}

		case isDone, ok := <-commDone:
			if ok {
				if isDone {
					break loop
				}
			}
		}

	}

}
