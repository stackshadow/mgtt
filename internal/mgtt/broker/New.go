package broker

import (
	"gitlab.com/mgtt/internal/mgtt-plugins/acl"
	"gitlab.com/mgtt/internal/mgtt-plugins/auth"
	"gitlab.com/mgtt/internal/mgtt/clientlist"
	"gitlab.com/mgtt/internal/mgtt/config"
	"gitlab.com/mgtt/internal/mgtt/pluginlist"
)

// New will create a new Broker
func New() (broker *Broker, err error) {
	broker = &Broker{
		clientEvents:                make(chan *Event, 10),
		pubrecs:                     make(map[uint16]Qos2),
		loopHandleResendPacketsExit: make(chan bool),
		clients:                     clientlist.Create(),
		plugins:                     pluginlist.Create(),
	}

	// auth
	authPlugin := auth.Create(broker)
	broker.plugins.Register(&authPlugin)
	// acl
	aclPlugin := acl.Create(broker)
	broker.RegisterPlugin(&aclPlugin)

	// client
	// this plugin is in development, so its disabled in the current release
	//clientPlugin := client.Create(broker)
	//broker.RegisterPlugin(&clientPlugin)

	// check if plugin struct exist
	if config.Globals.Plugins == nil {
		config.Globals.Plugins = make(map[string]interface{})
	}

	// inform all plugins about the configuration
	configChanged := broker.plugins.SendPluginConfig(config.Globals.Plugins)
	if configChanged {
		config.MustSave()
	}

	return
}
