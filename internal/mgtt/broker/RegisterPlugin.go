package broker

import "gitlab.com/mgtt/internal/mgtt/plugin"

func (broker *Broker) RegisterPlugin(newPlugin plugin.V2) {
	broker.plugins.Register(newPlugin)
}

func (broker *Broker) DeRegisterPlugin(name string) {
	broker.plugins.DeRegister(name)
}
