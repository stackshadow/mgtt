package broker

import (
	"github.com/eclipse/paho.mqtt.golang/packets"
)

// Publish will publish a message to all clients
func (broker *Broker) Publish(topic string, payload []byte, retain bool, QoS byte) (err error) {

	// construct the package
	pub := packets.NewControlPacket(packets.Publish).(*packets.PublishPacket)
	pub.MessageID = broker.lastID
	pub.Retain = retain
	pub.TopicName = topic
	pub.Payload = payload
	pub.Qos = QoS

	broker.lastID++

	_, _, err = broker.clients.PublishToAllClients(pub, "", false, broker.plugins.PublishToSubscriberPossible)
	return
}

// Publish will publish a message to all clients
func (broker *Broker) PublishToClient(clientID, topic string, payload []byte, retain bool, QoS byte) (err error) {
	return broker.clients.PublishToClient(clientID, topic, payload)
}
