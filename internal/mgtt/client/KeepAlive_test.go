package client

import (
	"errors"
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/eclipse/paho.mqtt.golang/packets"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/fails"
	"gitlab.com/mgtt/internal/mocked"
)

func TestKeepAlive(t *testing.T) {
	var readedPacket packets.ControlPacket
	var xType reflect.Type

	// setup logger
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Logger = log.Logger.With().Caller().Logger()

	var testClient *MgttClient = &MgttClient{}
	var netserver mocked.Con = mocked.ConNew()
	testClient.Init(netserver, 0)
	incomingPackages, done := testClient.Communicate()

	testClient.KeepAliveSetTimeout(5)

	time.Sleep(time.Second * 2)
	testClient.KeepAliveUpdate()
	time.Sleep(time.Second * 2)
	testClient.KeepAliveUpdate()

	// PingreqPacket
	testClient.SendPingreq()

	readedPacket, ok := <-incomingPackages
	if !ok {
		fails.PanicOnClientErr(errors.New("Channel error"), testClient.ID())
	}

	xType = reflect.TypeOf(readedPacket)
	if xType.String() != "*packets.PingreqPacket" {
		t.FailNow()
	}

	time.Sleep(time.Second * 10)

	testClient.SendPingreq()

	<-done

}
