package client

import (
	"io"

	"github.com/eclipse/paho.mqtt.golang/packets"
	"github.com/rs/zerolog/log"
)

// DelimReader takes an io.Reader and produces the contents of the reader
// on the returned channel. The contents on the channel will be returned
// on boundaries specified by the delim parameter, and will include this
// delimiter.
//
// If an error occurs while reading from the reader, the reading will end.
//
// In the case of an EOF or error, the channel will be closed.
//
// This must only be called once for any individual reader. The behavior is
// unknown and will be unexpected if this is called multiple times with the
// same reader.
func ReaderChannel(r io.Reader) <-chan packets.ControlPacket {

	// create new channel
	ch := make(chan packets.ControlPacket)

	// read-loop
	go func() {

		for {
			readedPacket, err := packets.ReadPacket(r)

			if err != nil {
				log.Error().Msg(err.Error())
				break
			}

			if readedPacket != nil {
				ch <- readedPacket
			}
		}

		close(ch)
	}()

	return ch
}
