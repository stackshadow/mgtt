package client

import (
	"errors"
	"os"
	"reflect"
	"testing"

	"github.com/eclipse/paho.mqtt.golang/packets"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/mgtt/internal/mgtt/fails"
	"gitlab.com/mgtt/internal/mocked"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

func TestSend(t *testing.T) {
	var err error
	var readedPacket packets.ControlPacket

	// setup logger
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Logger = log.Logger.With().Caller().Logger()

	var testClient *MgttClient = &MgttClient{}
	var netserver mocked.Con = mocked.ConNew()
	testClient.Init(netserver, 0)
	incomingPackages, _ := testClient.Communicate()

	testClient.SubScriptionAdd("/test/sensors/#")
	testClient.SubScriptionsAdd([]string{"/test/users/#", "/flat/sensors/#"})

	// ConnackPacket
	err = testClient.SendConnack(1, false)
	utils.PanicOnErr(err)

	readedPacket, ok := <-incomingPackages
	if !ok {
		fails.PanicOnClientErr(errors.New("Channel error"), testClient.ID())
	}

	xType := reflect.TypeOf(readedPacket)
	if xType.String() != "*packets.ConnackPacket" {
		t.FailNow()
	}

	// ConnectPacket
	testClient.SendConnect("username", "password", "clientid")

	readedPacket, ok = <-incomingPackages
	if !ok {
		fails.PanicOnClientErr(errors.New("Channel error"), testClient.ID())
	}

	xType = reflect.TypeOf(readedPacket)
	if xType.String() != "*packets.ConnectPacket" {
		t.FailNow()
	}

	// PingreqPacket
	testClient.SendPingreq()

	readedPacket, ok = <-incomingPackages
	if !ok {
		fails.PanicOnClientErr(errors.New("Channel error"), testClient.ID())
	}

	xType = reflect.TypeOf(readedPacket)
	if xType.String() != "*packets.PingreqPacket" {
		t.FailNow()
	}

	// PingrespPacket
	testClient.SendPingresp()

	readedPacket, ok = <-incomingPackages
	if !ok {
		fails.PanicOnClientErr(errors.New("Channel error"), testClient.ID())
	}

	xType = reflect.TypeOf(readedPacket)
	if xType.String() != "*packets.PingrespPacket" {
		t.FailNow()
	}

	// PubackPacket
	err = testClient.SendPuback(1)
	if err != nil {
		t.FailNow()
	}

	readedPacket, ok = <-incomingPackages
	if !ok {
		fails.PanicOnClientErr(errors.New("Channel error"), testClient.ID())
	}

	xType = reflect.TypeOf(readedPacket)
	if xType.String() != "*packets.PubackPacket" {
		t.FailNow()
	}

	// PubcompPacket
	err = testClient.SendPubcomp(1)
	if err != nil {
		t.FailNow()
	}

	readedPacket, ok = <-incomingPackages
	if !ok {
		fails.PanicOnClientErr(errors.New("Channel error"), testClient.ID())
	}

	xType = reflect.TypeOf(readedPacket)
	if xType.String() != "*packets.PubcompPacket" {
		t.FailNow()
	}
}
