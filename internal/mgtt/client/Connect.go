package client

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/url"

	"github.com/rs/xid"
	"gitlab.com/stackshadow/qommunicator/v2/pkg/utils"
)

// Connect will connect to an broker
//
// its a good idea to start the PingLoop after calling this function
func (client *MgttClient) Connect(clientURL string) {

	var err error

	// create a new clientID
	guid := xid.New()
	client.id = guid.String()

	// parse the url
	var serverURL *url.URL
	serverURL, err = url.Parse(clientURL)
	utils.PanicOnErr(err)

	// check schema
	if serverURL.Scheme == "tcp" {
		client.connection, err = net.Dial("tcp", serverURL.Host)
		utils.PanicOnErr(err)

		client.Connected = true
	} else if serverURL.Scheme == "tls" {
		conf := &tls.Config{
			//InsecureSkipVerify: true,
		}

		client.connection, err = tls.Dial("tcp", "127.0.0.1:443", conf)
		utils.PanicOnErr(err)

		client.Connected = true
	} else {
		err = fmt.Errorf("Unsupported scheme '%s' we only support ttls", serverURL.Scheme)
		utils.PanicOnErr(err)
	}

}
