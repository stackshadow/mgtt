with (import <nixpkgs> { });
let
  nixpkgs = import ./nixpkgs.nix;
  pkgs = import nixpkgs {
    system = "x86_64-linux";
  };
  package = ./.;
in
mkShell {
  buildInputs = [

  ];

  shellHook = ''
    export NIX_PATH="nixpkgs=${nixpkgs}:mgtt=${package}"
    export NIX_BUILD_CORES=4
  '';
}
