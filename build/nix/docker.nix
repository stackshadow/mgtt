{ system ? builtins.currentSystem
, pkgs ? import <nixpkgs> { inherit system; }
, binary
, version ? "dev"
}:
let
  lib = pkgs.lib;
  inherit (pkgs) dockerTools;
  inherit (lib) sourceByRegex;

in
#buildLayeredImage
dockerTools.streamLayeredImage {
  name = "mgtt";
  tag = version;
  maxLayers = 100;

  contents = with pkgs; [
    # basic stuff
    cacert

    # our binary
    binary
  ];

  created = "now";

  enableFakechroot = true;
  fakeRootCommands = ''
    ${dockerTools.shadowSetup}
    groupadd --gid 5005 mgtt
    useradd --groups mgtt --uid 5005 mgtt

    mkdir -p /tmp
    chmod a+rwX /tmp

    mkdir -p /data
    chown 5005 /data
  '';

  config = {
    User = "5005";
    Env = [
      "HOME=/tmp"
    ];
    ExposedPorts = {
      "1883/tcp" = { };
    };
    Entrypoint = [
      "${binary}/bin/mgtt"
    ];
    WorkingDir = "/data";
    Volumes = { "/data" = { }; };
  };
}
